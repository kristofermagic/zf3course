<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\ValueObject\MoneyAmount;
use Application\Service\Mailer;
use Application\ValueObject\EmailMessage;
use Application\Form\EmailForm;


class IndexController extends AbstractActionController
{
    private $mailer; 

    public function __construct( Mailer $mailer)
    {
        $this->mailer = $mailer;    
    }


    public function indexAction()
    {
        return new ViewModel();
    }
   
    public function sendMailAction()
    {
        // creating an Instance of the EmailForm()
        $form = new EmailForm();

        $result = '';       

        // checking if the call was using POST
        if ( $this->getRequest()->isPost()) {
            
            // retrieving params sent by POST
            $data = $this->params()->fromPost();

            $email   = $data['email'];
            $subject = $data['subject'];
            $message = $data['message'];

            // var_dump($data['date']);

            // Creating an OBJECT ( EmailMessage ) 
            $email = new EmailMessage( $email, $subject, $message );

            // Consuming the service MAILER 
            $result = $this->mailer->sendMail( $email ) ? 'Email has been sent' : 'Email could not be sent';
        }

        return new ViewModel([
                'response' => $result,
                'form' => $form
            ]
        );
    }



    public function aboutAction() 
    {
        $moneyAmountObj  = new MoneyAmount( 10.6 );

        return new ViewModel(
            [
                'objMA' => $moneyAmountObj
            ]
        );
    }
}

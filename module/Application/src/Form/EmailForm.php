<?php

namespace Application\Form;

use Zend\Form\Form;

// A feedback form model
class EmailForm extends Form
{
  // Constructor.   
  public function __construct()
  {
    // Define form name
    parent::__construct('email-form');

    // Set POST method for this form
    $this->setAttribute('method', 'post');
  
    // (Optionally) set action for this form
    // $this->setAttribute('action', '/contactus');
  
    // Create the form fields here ...	    
    $this->addElements();
    $this->addInputFilter();

  }

  public function addElements() 
  {
    
       // Add "email" field
        $this->add([
          'type'  => 'email',        // Element type
          'name' => 'email',      // Field name
          'attributes' => [         // Array of attributes
            'id'  => 'email',  
            'placeholder' => 'ex: jhondoe@example.com' 
        ],
        'options' => [            // Array of options
           'label' => 'Email',  // Text label
        ],
      ]); 

      // Add "subject" field
      $this->add([
            'type'  => 'text',        // Element type
            'name' => 'subject',      // Field name
            'required' => true,
            'attributes' => [         // Array of attributes
                'id'  => 'subject', 
                'placeholder' => 'write a subjet here' 

        ],
        'options' => [            // Array of options
            'label' => 'Subject',  // Text label
        ],
      ]); 
      // Add "body" field
      $this->add([
            'type'  => 'textarea',        // Element type
            'name' => 'message',      // Field name
            'attributes' => [         // Array of attributes
                'id'  => 'message',   
                'placeholder' => 'write a message here'  
        ],
        'options' => [            // Array of options
            'label' => 'Message',  // Text label
        ],
      ]); 
      
      // Add "body" field
      $this->add([
            'type'  => 'submit',        // Element type
            'name' => 'submit',      // Field name
            'attributes' => [         // Array of attributes
            'value'  => 'Submit',     
        ],
        'options' => [            // Array of options
            'label' => 'Send',  // Text label
        ],
      ]); 
  }

  // This method creates input filter (used for form filtering/validation).
  private function addInputFilter() 
  {
        // Get the default input filter attached to form model.
        $inputFilter = $this->getInputFilter();
            
        $inputFilter->add([
            'name'     => 'email',
            'required' => true,
            'filters'  => [
            ['name' => 'StringTrim'],                    
            ],                
            'validators' => [
            [
                'name' => 'EmailAddress',
                'options' => [
                'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                'useMxCheck' => false,                            
                ],
            ],
            ],
        ]);
            
        $inputFilter->add([
            'name'     => 'subject',
            'required' => true,
            'filters'  => [
            ['name' => 'StringTrim'],
            ['name' => 'StripTags'],
            ['name' => 'StripNewlines'],
            ],                
            'validators' => [
            [
                'name' => 'StringLength',
                'options' => [
                    'min' => 1,
                    'max' => 128
                ],
            ],
            ],
        ]);
        
        $inputFilter->add([
            'name'     => 'message',
            'required' => true,
            'filters'  => [                    
                ['name' => 'StripTags'],
            ],                
            'validators' => [
            [
                'name' => 'StringLength',
                'options' => [
                    'min' => 1,
                    'max' => 4096
                ],
            ],
            ],
        ]);                
    } //End Filters
  

}
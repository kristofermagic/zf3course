<?php

namespace Application\Service;

// The Mailer service, which can send messages by E-mail
class Mailer 
{
  
    public static function sendMail($message) 
    {
        // Use PHP mail() function to send an E-mail
        if(!mail($message->getRecipient(), 
             $message->getSubject(), 
             $message->getText())) 
        {
            // Error sending message
            return false; 
        }
    
        return true;
    }
}
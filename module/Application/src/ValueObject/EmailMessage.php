<?php

    namespace Application\ValueObject;

    
// The Email message value object
class EmailMessage 
{  
    private $recipient;
    private $subject;
    private $text;  
  
    // Constructor
    public function __construct($recipient, $subject, $text) 
    {
        $this->recipient = $recipient;
        $this->subject = $subject;
        $this->text = $text;
    }
  
    // Getters
    public function getRecipient() 
    {
        return $this->recipient;
    }
  
    public function getSubject() 
    {
        return $this->subject;
    }
  
    public function getText() 
    {
        return $this->text;
    }
}

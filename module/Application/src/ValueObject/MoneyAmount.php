<?php

namespace Application\ValueObject;

class MoneyAmount 
{
    // Properties
    private $currency;
    private $amount;
  
    // Constructor
    public function __construct($amount, $currency='USD') 
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }
  
    // Gets the currency code  
    public function getCurrency() 
    {
        return $this->currency;  
    }
  
    // Gets the money amount
    public function getAmount() 
    {
        return $this->amount;
    }  
}